#! /usr/bin/env node
var banish = require('to-zalgo/banish');

process.argv.slice(2).forEach(element => {
    console.log(banish(element));
});
